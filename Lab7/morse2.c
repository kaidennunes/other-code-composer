#include "msp430.h"          // .cdecls C,"msp430.h"
#include "morse2.h"
#include <stdlib.h>
#include <ctype.h>

/*
 * main.c
 *
 *
 *	Kaiden Nunes
 * This is my work
 */
//; Global variables ------------------------------------------------------------
extern int beep_cnt;								//            .bss    beep_cnt,2              ; beeper flag
extern int delay_cnt;								//            .bss    delay_cnt,2             ; delay flag
extern int debounce_cnt;							//            .bss    debounce_cnt,2        	; debounce count
extern int second_cnt;								//            .bss	second_cnt,2			; second count

extern char* letters[];
extern char* numbers[];

extern doSpace(void);
extern doEnd(void);
extern doDash(void);
extern doDot(void);

extern int main_asm(void);
int main(void)
{
	char message[] = "HELLO CS 124 WORLD";
    WDTCTL = WDT_CTL;						//    main_asm:   mov.w   #WDT_CTL,&WDTCTL        ; set WD timer interval
    IE1 = WDTIE;							//                mov.b   #WDTIE,&IE1             ; enable WDT interrupt
    TACTL = WDT_CTL;						//                mov.w   #WDT_CTL,&TACTL         ; Set timer interval
    P1SEL &= ~0x0f;							//                bic.b   #0x0f,&P1SEL            ; RBX430-1 push buttons
    P1DIR &= ~0x0f;							//            	bic.b   #0x0f,&P1DIR            ; Configure P1.0-3 as Inputs
    P1OUT = 0x0f;							//               	bis.b   #0x0f,&P1OUT            ; pull-ups
    P1IES = 0x0f;							//               	bis.b   #0x0f,&P1IES            ; h to l
    P1REN = 0x0f;							//               	bis.b   #0x0f,&P1REN            ; enable pull-ups
    P1IE = 0x0f;							//               	bis.b   #0x0f,&P1IE             ; enable switch interrupts
    P3DIR = 0x10;							//                bis.b 	#0x10,&P3DIR           	; set P3.4 as output
    P4DIR = 0x60;							//                bis.b 	#0x60,&P4DIR           	; set P4.5-6 as output
    beep_cnt = 0;							//                clr.w   &beep_cnt               ; clear counters
    delay_cnt = 0;							//                clr.w   &delay_cnt
    second_cnt = WDT_IPS;					//                mov.w	#WDT_IPS,&second_cnt	; set timer
    _BIS_SR(GIE);							//                bis.w   #GIE,SR                 ; enable interrupts


          while (1)
          {								 	//    loop:     	mov.w  	#message,r4            	; point to message
        	  char c;
        	  char* cptr;
        	  char* mptr = message;
											//
        	  while (c = *mptr++)								//    loop02:   	mov.b 	@r4+,r5                	; get character
        	  {
											//
											//
											//    			cmp.b 	#32,r5					; is space
											//    				jne determine				; y
											//    			call 	#doSpace				; make space
											//    			jmp		loop02					; get next char
        		  if (isspace(c))
        		  {
        			  doSpace();
        			  continue;
        		  }
											//    determine:	cmp.b	#60,r5					; ascii code
											//    				jge isLetter				; greater? Then letter
											//    				jl	isNumber				; less? Then number
											//



											//    isLetter:	sub.w  #'A',r5                	; make index 0-25
											//    		   	add.w  r5,r5                  	; make word index
											//    		   	mov.w  letters(r5),r5         	; get pointer to letter codes
											//    			jmp loop10
        		  else if (isalpha(c)) cptr = letters[toupper(c) - 'A'];


											//    isNumber:	sub.w  #'0',r5                	; make index 0-9
											//    		   	add.w  r5,r5                  	; make word index
											//    		   	mov.w  numbers(r5),r5         	; get pointer to letter codes
											//    			jmp loop10
        		  else if (isdigit(c)) cptr = numbers[c - '0'];
        		  int code;
        		  while (code = *cptr++)
        		  {
        			  	 if(code == DOT)
        			  	 {

											//
											//    loop10:   	mov.b  @r5+,r6              	; get DOT, DASH, or END
								doDot();	//    		   	cmp.b  #DOT,r6             	  	; dot?
											//    				jne isDash					; n, skip to dash
											//    			call	#doDot					; make dot
											//    			jmp		loop10					; get next letter
        			  	 }
        			  	 else if(code == DASH)
        			  	 {
											//    isDash:		cmp.b  #DASH,r6             	; dash?
											//    				jne isEnd					; n, skip to end
        			  		 doDash();		//    			call	#doDash					; make dash
											//    			jmp		loop10					; get next letter

        			  	 }
        		  }

											//    isEnd:		cmp.b  #END,r6             	  	; end?
											//    				jne repeat					; n repeat message
        		  	  	  	  	  	  	  	//    			call	#doEnd					; end spacing
											//    			jmp		loop02					; y, get char
        		  doEnd();
        	  }
          }									//    repeat:		jmp		loop				; repeat the message
    return 0;
}
//; P1 interrupt service---------------------------------------------------------
#pragma vector=PORT1_VECTOR
__interrupt void Port_1_ISR(void)
{                         // P1_ISR:
   P1IFG &= ~0x0f;        //   bic.b #0x0f,&P1IFG  ; acknowledge
   debounce_cnt = DEBOUNCE;   //   mov.w #DB_CNT,&dcnt ; start debounce
   return;                //   reti
}
