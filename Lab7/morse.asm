			.title	"morse.asm"
;*******************************************************************************
;     Project:  morse.asm
;      Author:  Kaiden Nunes
;
;		This is my work, and nothing but my work (except the parts that were already there;))
;
; Description:  Outputs a message in Morse Code using a LED and a transducer
;               (speaker).  The watchdog is configured as an interval timer.
;               The watchdog interrupt service routine (ISR) toggles the green
;               LED every second and pulse width modulates (PWM) the speaker
;               such that a tone is produced.
;
;	Morse code is composed of dashes and dots:
;
;        1. A dot is equal to an element of time.
;        2. One dash is equal to three dots.
;        3. The space between parts of the letter is equal to one dot.
;        4. The space between two letters is equal to three dots.
;        5. The space between two words is equal to seven dots.
;
;    5 WPM = 60 sec / (5 * 50) elements = 240 milliseconds per element.
;    element = (WDT_IPS * 6 / WPM) / 5
;
;******************************************************************************
			.def  doDash
			.def  doDot
			.def  doEnd
			.def  doSpace

 			.asg   "bis.b #0x40,&P4OUT",RED_ON
	        .asg   "bic.b #0x40,&P4OUT",RED_OFF

	        .asg   "xor.b #0x10,&P3OUT",GREEN_TOGGLE

; System equates --------------------------------------------------------------
            .cdecls C,"msp430.h"            ; include c header
            .cdecls C,"morse2.h"			; include morse code header

; External references ---------------------------------------------------------
            .ref    numbers                 ; codes for 0-9
            .ref    letters                 ; codes for A-Z
                            
; Global variables ------------------------------------------------------------
			.def	beep_cnt
            .bss    beep_cnt,2              ; beeper flag
            .def 	delay_cnt
            .bss    delay_cnt,2             ; delay flag
            .def 	debounce_cnt
            .bss    debounce_cnt,2        	; debounce count
            .def 	second_cnt
            .bss	second_cnt,2			; second count

; Do Dot Subroutine ----------------------------------------------------------

doDot:		push 	r15						; Push r15 on stack
			mov.w   #ELEMENT,r15            ; output DOT
            call    #beep
            mov.w   #ELEMENT,r15            ; delay 1 element
            call    #delay
            pop		r15						; Restore r15
            ret

; Do Dash Subroutine ----------------------------------------------------------

doDash:		push 	r15						; Push r15 on stack
			mov.w   #ELEMENT*3,r15          ; output DASH
            call    #beep
            mov.w   #ELEMENT,r15            ; delay 1 element
            call    #delay
            pop		r15						; Restore r15
            ret

; Do Space Subroutine ----------------------------------------------------------
doSpace:	push	r15						; push r15 on stack
            mov.w   #ELEMENT*4,r15          ; output space
            call    #delay                  ; delay
       	    pop		r15						; Restore r15
            ret

; Do End Subroutine ----------------------------------------------------------
doEnd:		push	r15						; push r15 on stack
            mov.w   #ELEMENT*3,r15          ; delay
            call    #delay                  ; delay
       	    pop		r15						; Restore r15
            ret

; beep (r15) ticks subroutine -------------------------------------------------
beep:       mov.w   r15,&beep_cnt           ; start beep

beep02:     tst.w   &beep_cnt               ; beep finished?
              jne   beep02                  ; n
            ret                             ; y


; delay (r15) ticks subroutine ------------------------------------------------
delay:      mov.w   r15,&delay_cnt          ; start delay

delay02:    tst.w   &delay_cnt              ; delay done?
              jne   delay02                 ; n
            ret                             ; y


; Watchdog Timer ISR ----------------------------------------------------------
WDT_ISR:    tst.w   &beep_cnt               ; beep on?
              jeq   WDT_02                  ; n
            RED_ON							; Turn on red led
            dec.w   &beep_cnt               ; y, decrement count
            xor.b   #0x20,&P4OUT            ; beep using 50% PWM

WDT_02:   	RED_OFF							; Turn off red led
			tst.w   &delay_cnt              ; delay?
              jeq   WDT_03                  ; n
            dec.w   &delay_cnt              ; y, decrement count


WDT_03:		dec.w	&second_cnt				; second over?
				jne	WDT_04					; n
			mov.w	#WDT_IPS,&second_cnt	; y, reset timer
			GREEN_TOGGLE					; toggle green led

WDT_04:		tst.w   debounce_cnt           ; debouncing?
			  jeq   WDT_10                 ; n
          	dec.w   debounce_cnt           ; y, decrement count, done?
              jne   WDT_10                 ; n
           	push    r15                    ; y
           	mov.b   &P1IN,r15              ; read switches
           	and.b   #0x0f,r15
           	xor.b   #0x0f,r15              ; any switches?
              jeq   WDT_20                 ; n

			; process switch inputs (r15 = switches)
			xor.b   #0x20,&P4DIR           ; Toggles speaker after debouncing


WDT_20:    pop     r15

WDT_10:     reti                            ; return from interrupt

; Interrupt Vectors -----------------------------------------------------------
            .sect   ".int10"                ; Watchdog Vector
            .word   WDT_ISR                 ; Watchdog ISR
