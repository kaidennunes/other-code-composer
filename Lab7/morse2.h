/*
 * morse2.h
 *
 *  Created on: Mar 14, 2015
 *      Author: Kaiden
 */

#ifndef MORSE2_H_
#define MORSE2_H_

// System equates --------------------------------------------------------------
#define myCLOCK 1200000								//myCLOCK     .equ    1200000                 ; 1.2 Mhz clock
#define WDT_CTL WDT_MDLY_0_5						//WDT_CTL     .equ    WDT_MDLY_0_5            ; WD: Timer, SMCLK, 0.5 ms
#define WDT_CPI 500									//WDT_CPI     .equ    500                     ; WDT Clocks Per Interrupt (@1 Mhz)
#define WDT_IPS (myCLOCK/WDT_CPI)					//WDT_IPS     .equ    (myCLOCK/WDT_CPI)       ; WDT Interrupts Per Second
#define STACK 0x0600								//STACK       .equ    0x0600                  ; top of stack
#define DEBOUNCE 10									//DEBOUNCE    .equ    10					  ; debounce reset

//; Morse Code equates ----------------------------------------------------------
#define END 0										//END         .equ    0
#define DOT 1										//DOT         .equ    1
#define	DASH 2										//DASH        .equ    2
#define ELEMENT ((WDT_IPS*240)/1000)				//ELEMENT     .equ    ((WDT_IPS*240)/1000)    ; (WDT_IPS * 6 / WPM) / 5



#endif /* MORSE2_H_ */
