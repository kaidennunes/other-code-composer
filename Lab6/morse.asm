			.title	"morse.asm"
;*******************************************************************************
;     Project:  morse.asm
;      Author:  Kaiden Nunes
;
;		This is my work, and nothing but my work (except the parts that were already there;))
;
; Description:  Outputs a message in Morse Code using a LED and a transducer
;               (speaker).  The watchdog is configured as an interval timer.
;               The watchdog interrupt service routine (ISR) toggles the green
;               LED every second and pulse width modulates (PWM) the speaker
;               such that a tone is produced.
;
;	Morse code is composed of dashes and dots:
;
;        1. A dot is equal to an element of time.
;        2. One dash is equal to three dots.
;        3. The space between parts of the letter is equal to one dot.
;        4. The space between two letters is equal to three dots.
;        5. The space between two words is equal to seven dots.
;
;    5 WPM = 60 sec / (5 * 50) elements = 240 milliseconds per element.
;    element = (WDT_IPS * 6 / WPM) / 5
;
;******************************************************************************


 			.asg   "bis.b #0x40,&P4OUT",RED_ON
	        .asg   "bic.b #0x40,&P4OUT",RED_OFF

	        .asg   "xor.b #0x10,&P3OUT",GREEN_TOGGLE

; System equates --------------------------------------------------------------
            .cdecls C,"msp430.h"            ; include c header
myCLOCK     .equ    1200000                 ; 1.2 Mhz clock
WDT_CTL     .equ    WDT_MDLY_0_5            ; WD: Timer, SMCLK, 0.5 ms
WDT_CPI     .equ    500                     ; WDT Clocks Per Interrupt (@1 Mhz)
WDT_IPS     .equ    (myCLOCK/WDT_CPI)       ; WDT Interrupts Per Second
STACK       .equ    0x0600                  ; top of stack
DEBOUNCE  	.equ    10						; debounce reset

; Morse Code equates ----------------------------------------------------------
END         .equ    0
DOT         .equ    1
DASH        .equ    2
ELEMENT     .equ    ((WDT_IPS*240)/1000)    ; (WDT_IPS * 6 / WPM) / 5

; External references ---------------------------------------------------------
            .ref    numbers                 ; codes for 0-9
            .ref    letters                 ; codes for A-Z
                            
; Global variables ------------------------------------------------------------
            .bss    beep_cnt,2              ; beeper flag
            .bss    delay_cnt,2             ; delay flag
            .bss    debounce_cnt,2        	; debounce count
            .bss	second_cnt,2			; second count

; Program section -------------------------------------------------------------
            .text                           ; program section
message:    .string "HELLO CS 124 WORLD"    ; message
            .byte   0
            .align  2                       ; align on word boundary

; power-up reset --------------------------------------------------------------
RESET:      mov.w   #STACK,SP               ; initialize stack pointer
            call    #main_asm               ; call main function
            jmp     $                       ; you should never get here!

; start main function vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv	
main_asm:   mov.w   #WDT_CTL,&WDTCTL        ; set WD timer interval
            mov.b   #WDTIE,&IE1             ; enable WDT interrupt
            mov.w   #WDT_CTL,&TACTL         ; Set timer interval
            bic.b   #0x0f,&P1SEL            ; RBX430-1 push buttons
        	bic.b   #0x0f,&P1DIR            ; Configure P1.0-3 as Inputs
           	bis.b   #0x0f,&P1OUT            ; pull-ups
           	bis.b   #0x0f,&P1IES            ; h to l
           	bis.b   #0x0f,&P1REN            ; enable pull-ups
           	bis.b   #0x0f,&P1IE             ; enable switch interrupts
            bis.b 	#0x10,&P3DIR           	; set P3.4 as output
            bis.b 	#0x40,&P4DIR           	; set P4.6 as output
            bis.b   #0x20,&P4DIR            ; set P4.5 as output (speaker)
            clr.w   &beep_cnt               ; clear counters
            clr.w   &delay_cnt
            mov.w	#WDT_IPS,&second_cnt	; set timer
            bis.w   #GIE,SR                 ; enable interrupts

loop:     	mov.w  	#message,r4            	; point to message

loop02:   	mov.b 	@r4+,r5                	; get character


			cmp.b 	#32,r5					; is space
				jne determine				; y
			call 	#doSpace				; make space
			jmp		loop02					; get next char

determine:	cmp.b	#60,r5					; ascii code
				jge isLetter				; greater? Then letter
				jl	isNumber				; less? Then number


isLetter:	sub.w  #'A',r5                	; make index 0-25
		   	add.w  r5,r5                  	; make word index
		   	mov.w  letters(r5),r5         	; get pointer to letter codes
			jmp loop10

isNumber:	sub.w  #'0',r5                	; make index 0-9
		   	add.w  r5,r5                  	; make word index
		   	mov.w  numbers(r5),r5         	; get pointer to letter codes
			jmp loop10


loop10:   	mov.b  @r5+,r6              	; get DOT, DASH, or END
		   	cmp.b  #DOT,r6             	  	; dot?
				jne isDash					; n, skip to dash
			call	#doDot					; make dot
			jmp		loop10					; get next letter
isDash:		cmp.b  #DASH,r6             	; dash?
				jne isEnd					; n, skip to end
			call	#doDash					; make dash
			jmp		loop10					; get next letter
isEnd:		cmp.b  #END,r6             	  	; end?
				jne repeat					; n repeat message
			call	#doEnd					; end spacing
			jmp		loop02					; y, get char


repeat:		jmp		loop					; repeat the message
; end main function ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

; Do Dot Subroutine ----------------------------------------------------------

doDot:		push 	r15						; Push r15 on stack
			mov.w   #ELEMENT,r15            ; output DOT
            call    #beep
            mov.w   #ELEMENT,r15            ; delay 1 element
            call    #delay
            pop		r15						; Restore r15
            ret

; Do Dash Subroutine ----------------------------------------------------------

doDash:		push 	r15						; Push r15 on stack
			mov.w   #ELEMENT*3,r15          ; output DASH
            call    #beep
            mov.w   #ELEMENT,r15            ; delay 1 element
            call    #delay
            pop		r15						; Restore r15
            ret

; Do Space Subroutine ----------------------------------------------------------
doSpace:	push	r15						; push r15 on stack
            mov.w   #ELEMENT*7,r15          ; output space
            call    #delay                  ; delay
       	    pop		r15						; Restore r15
            ret

; Do End Subroutine ----------------------------------------------------------
doEnd:		push	r15						; push r15 on stack
            mov.w   #ELEMENT*3,r15          ; delay
            call    #delay                  ; delay
       	    pop		r15						; Restore r15
            ret

; beep (r15) ticks subroutine -------------------------------------------------
beep:       mov.w   r15,&beep_cnt           ; start beep

beep02:     tst.w   &beep_cnt               ; beep finished?
              jne   beep02                  ; n
            ret                             ; y


; delay (r15) ticks subroutine ------------------------------------------------
delay:      mov.w   r15,&delay_cnt          ; start delay

delay02:    tst.w   &delay_cnt              ; delay done?
              jne   delay02                 ; n
            ret                             ; y


; Watchdog Timer ISR ----------------------------------------------------------
WDT_ISR:    tst.w   &beep_cnt               ; beep on?
              jeq   WDT_02                  ; n
            RED_ON							; Turn on red led
            dec.w   &beep_cnt               ; y, decrement count
            xor.b   #0x20,&P4OUT            ; beep using 50% PWM

WDT_02:   	RED_OFF							; Turn off red led
			tst.w   &delay_cnt              ; delay?
              jeq   WDT_03                  ; n
            dec.w   &delay_cnt              ; y, decrement count


WDT_03:		dec.w	&second_cnt				; second over?
				jne	WDT_04					; n
			mov.w	#WDT_IPS,&second_cnt	; y, reset timer
			GREEN_TOGGLE					; toggle green led

WDT_04:		tst.w   debounce_cnt           ; debouncing?
			  jeq   WDT_10                 ; n
          	dec.w   debounce_cnt           ; y, decrement count, done?
              jne   WDT_10                 ; n
           	push    r15                    ; y
           	mov.b   &P1IN,r15              ; read switches
           	and.b   #0x0f,r15
           	xor.b   #0x0f,r15              ; any switches?
              jeq   WDT_20                 ; n

			; process switch inputs (r15 = switches)
			xor.b   #0x20,&P4DIR           ; Toggles speaker after debouncing


WDT_20:    pop     r15

WDT_10:     reti                            ; return from interrupt

; P1 interrupt service---------------------------------------------------------
P1_ISR:    bic.b   #0x0f,&P1IFG           ; acknowledge (put hands down)
           mov.w   #DEBOUNCE,debounce_cnt ; reset debounce count
           reti

; Interrupt Vectors -----------------------------------------------------------
            .sect   ".int10"                ; Watchdog Vector
            .word   WDT_ISR                 ; Watchdog ISR

 			.sect  ".int02"              	; P1 interrupt vector
           	.word  P1_ISR					; P1 ISR

            .sect   ".reset"                ; PUC Vector
            .word   RESET                   ; RESET ISR
            .end
