;*******************************************************************************
;   Lab 5b - traffic.asm
;
;   Description:  1. Turn the large green LED and small red LED on and
;                    delay 20 seconds while checking for orange LED.
;                    (If orange LED is on and 10 seconds has expired, immediately
;                    skip to next step.)
;                 2. Turn large green LED off and yellow LED on for 5 seconds.
;                 3. Turn yellow LED off and large red LED on.
;                 4. If orange LED is on, turn small red LED off and small green
;                    LED on.  After 5 seconds, toggle small green LED on and off
;                    for 6 seconds at 1 second intervals.  Finish by toggling
;                    small green LED on and off for 4 seconds at 1/5 second
;                    intervals.
;                    Else, turn large red LED on for 5 seconds.
;                 5. Repeat the stoplight cycle.
;
;   I certify this to be my source code and not obtained from any student, past
;   or current.
;			- Kaiden Nunes
;
;*******************************************************************************
;                            MSP430F2274
;                  .-----------------------------.
;            SW1-->|P1.0^                    P2.0|<->LCD_DB0
;            SW2-->|P1.1^                    P2.1|<->LCD_DB1
;            SW3-->|P1.2^                    P2.2|<->LCD_DB2
;            SW4-->|P1.3^                    P2.3|<->LCD_DB3
;       ADXL_INT-->|P1.4                     P2.4|<->LCD_DB4
;        AUX INT-->|P1.5                     P2.5|<->LCD_DB5
;        SERVO_1<--|P1.6 (TA1)               P2.6|<->LCD_DB6
;        SERVO_2<--|P1.7 (TA2)               P2.7|<->LCD_DB7
;                  |                             |
;         LCD_A0<--|P3.0                     P4.0|-->LED_1 (Green)
;        i2c_SDA<->|P3.1 (UCB0SDA)     (TB1) P4.1|-->LED_2 (Orange) / SERVO_3
;        i2c_SCL<--|P3.2 (UCB0SCL)     (TB2) P4.2|-->LED_3 (Yellow) / SERVO_4
;         LCD_RW<--|P3.3                     P4.3|-->LED_4 (Red)
;   TX/LED_5 (G)<--|P3.4 (UCA0TXD)     (TB1) P4.4|-->LCD_BL
;             RX-->|P3.5 (UCA0RXD)     (TB2) P4.5|-->SPEAKER
;           RPOT-->|P3.6 (A6)          (A15) P4.6|-->LED 6 (R)
;           LPOT-->|P3.7 (A7)                P4.7|-->LCD_E
;                  '-----------------------------'
;
;*******************************************************************************
;*******************************************************************************
            .cdecls  C,LIST,"msp430.h"      ; MSP430

            .asg   "bis.b #0x08,&P4OUT",RED_ON
            .asg   "bic.b #0x08,&P4OUT",RED_OFF
            .asg   "xor.b #0x08,&P4OUT",RED_TOGGLE
            .asg   "bit.b #0x08,&P4OUT",RED_TEST

	        .asg   "bis.b #0x04,&P4OUT",YELLOW_ON
	        .asg   "bic.b #0x04,&P4OUT",YELLOW_OFF
	        .asg   "xor.b #0x04,&P4OUT",YELLOW_TOGGLE
	        .asg   "bit.b #0x04,&P4OUT",YELLOW_TEST

	        .asg   "bis.b #0x02,&P4OUT",ORANGE_ON
	        .asg   "bic.b #0x02,&P4OUT",ORANGE_OFF
	        .asg   "xor.b #0x02,&P4OUT",ORANGE_TOGGLE
	        .asg   "bit.b #0x02,&P4OUT",ORANGE_TEST

	        .asg   "bis.b #0x01,&P4OUT",GREEN_ON
	        .asg   "bic.b #0x01,&P4OUT",GREEN_OFF
	        .asg   "xor.b #0x01,&P4OUT",GREEN_TOGGLE
	        .asg   "bit.b #0x01,&P4OUT",GREEN_TEST

	        .asg   "bis.b #0x40,&P4OUT",RED2_ON
	        .asg   "bic.b #0x40,&P4OUT",RED2_OFF
	        .asg   "xor.b #0x40,&P4OUT",RED2_TOGGLE
	        .asg   "bit.b #0x40,&P4OUT",RED2_TEST

	        .asg   "bis.b #0x10,&P3OUT",GREEN2_ON
	        .asg   "bic.b #0x10,&P3OUT",GREEN2_OFF
	        .asg   "xor.b #0x10,&P3OUT",GREEN2_TOGGLE
	        .asg   "bit.b #0x10,&P3OUT",GREEN2_TEST

;COUNT       						.equ    11367
COUNT				      		 	.equ    35700
GREEN_COUNT 						.equ    200
YELLOW_COUNT						.equ    50
RED_COUNT							.equ    50
PEDESTRIAN_GREEN_COUNT				.equ    50
PEDESTRIAN_SLOWGREEN_COUNT			.equ    6
PEDESTRIAN_FASTGREEN_COUNT			.equ    20
PEDESTRIAN_SLOWGREEN_TOGGLE_COUNT	.equ    10
PEDESTRIAN_FASTGREEN_TOGGLE_COUNT	.equ    2

;------------------------------------------------------------------------------
            .text                           ; beginning of executable code
            .retain                         ; Override ELF conditional linking
;-------------------------------------------------------------------------------
start:      mov.w   #__STACK_END,SP         ; init stack pointer
            mov.w   #WDTPW+WDTHOLD,&WDTCTL  ; stop WDT
			bis.b  #0x4f,&P4DIR            	; set P4.0-3,6 as output
        	bis.b  #0x10,&P3DIR           	; set P3.4 as output
        	bis.w  #GIE,SR              	; enable general interrupts
        	bic.b  #0x0f,&P1SEL        		; select GPIO
        	bic.b  #0x0f,&P1DIR         	; configure P1.0-3 as inputs
         	bis.b  #0x0f,&P1OUT          	; use pull-up
          	bis.b  #0x0f,&P1REN          	; enable pull-up
          	bis.b  #0x0f,&P1IES          	; trigger on high to low transition
          	bis.b  #0x0f,&P1IE           	; P1.0-3 interrupt enabled
          	bic.b  #0x0f,&P1IFG          	; P1.0-3 IFG cleared

mainloop:
			ORANGE_OFF						; Turn off all lights
        	GREEN_OFF						; Turn off all lights
        	RED_OFF							; Turn off all lights
        	YELLOW_OFF						; Turn off all lights
			mov.w   #COUNT,r15              ; use R15 as delay counter
            mov.w   #GREEN_COUNT,r14        ; use R14 as green delay counter
            mov.w   #YELLOW_COUNT,r13       ; use R13 as green delay counter
            mov.w   #RED_COUNT,r12    	    ; use R12 as green delay counter

greentoggle: GREEN2_OFF						; turn off pedestrian green
			 RED2_ON						; turn on pedestrian red
			 GREEN_ON						; turn on green led
greendelay:	call #delayloop					; Call tenth of second subroutine
			sub.w	#1,r14                  ; delay over?
            jne	greendelay		            ; n
            GREEN_OFF						; turn off green led

yellowtoggle: YELLOW_TOGGLE					; turn on yellow led
yellowdelay:  call #delayloop				; Call tenth of second subroutine
			sub.w	#1,r13                  ; delay over?
            jne	yellowdelay		            ; n
            YELLOW_OFF						; turn off yellow led

redtoggle:	ORANGE_TEST						; is orange led on?
			jne pedestrian					; y go to pedestrian state
			RED_ON							; n, turn on red led and state
reddelay:	call #delayloop					; Call tenth of second subroutine
			sub.w	#1,r12                  ; delay over?
            jne	reddelay		            ; n
            RED_OFF							; turn off red led

            jmp	mainloop              		; return to main loop
;------------------------------------------------------------------------------
;			Pedestrian state
;------------------------------------------------------------------------------
pedestrian:	RED_ON							; turn on red led
			ORANGE_OFF						; Turn off orange led
			RED2_OFF						; turn off pedestrian red
			GREEN2_ON						; turn on pedestrian green
			mov.w   #PEDESTRIAN_GREEN_COUNT,r11      ; use R11 as delay counter

normalpedestrian:
			call #delayloop					; Call tenth of second subroutine
			sub.w	#1,r11                  ; delay over?
            jne	normalpedestrian		            ; n

			mov.w   #PEDESTRIAN_SLOWGREEN_COUNT,r11      ; use R11 as delay counter
fasterpedestrian:
			GREEN2_TOGGLE					; Toggle pedestrian green
			mov.w   #PEDESTRIAN_SLOWGREEN_TOGGLE_COUNT,r10      ; use R10 as delay counter
fasterpedestriantoggle:
			call #delayloop					; Call tenth of second subroutine
			sub.w	#1,r10                  ; delay over?
			jne	fasterpedestriantoggle		; n
			sub.w	#1,r11                  ; delay over?
            jne	fasterpedestrian		    ; n

			mov.w   #PEDESTRIAN_FASTGREEN_COUNT,r11      ; use R11 as delay counter
fastestpedestrian:
			GREEN2_TOGGLE					; Toggle pedestrian green
			mov.w   #PEDESTRIAN_FASTGREEN_TOGGLE_COUNT,r10      ; use R10 as delay counter
fastestpedestriantoggle:
			call #delayloop					; Call tenth of second subroutine
			sub.w	#1,r10                  ; delay over?
			jne	fastestpedestriantoggle		; n
			sub.w	#1,r11                  ; delay over?
            jne	fastestpedestrian	        ; n
			jmp mainloop					; return to mainloop

;------------------------------------------------------------------------------
;			1/10 of a second delay subroutine
;------------------------------------------------------------------------------
delayloop:	PUSH R15						; 	Delay count data
delaying:
			sub.w	#1,r15                  ;    delay over? 1w,1c
            jne	delaying	        	    ;    n 1w, 2c
            POP R15							; 	 Restore R15
            ret				                ;    return to loop 1w, 2c
;------------------------------------------------------------------------------
;			P1 interrupt service
;------------------------------------------------------------------------------
P1_ISR:
          bic.b  #0x0f,&P1IFG      		    ; clear P1.0-3 Interrupt Flag
       		  ORANGE_ON			          		; turn on orange LED
          reti

;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect 	.stack
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;           Interrupt Vectors
;------------------------------------------------------------------------------
     	    .sect  ".int02"             		 ; P1 interrupt vector
      	    .word  P1_ISR
            .sect   ".reset"                ; MSP430 RESET Vector
            .word   start                   ; start address
            .end
