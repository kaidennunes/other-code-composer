//	life.h	03/23/2015
//*******************************************************************************

#ifndef LIFE_H_
#define LIFE_H_

#define myCLOCK			16000000			// 1.2 Mhz clock
#define CLOCK			_16MHZ
#define cell_birth(row,col) (life[(row)][(col) >> 3] |= (0x80 >> ((col) & 0x07)))
#define cell_death(row,col) (life[(row)][(col) >> 3] &= ~(0x80 >> ((col) & 0x07)))

#define ptest_cell_nc(life_pr,col) ((life_pr[(col+1) >> 3] & (0x80 >> ((col+1) & 0x07)) ? 1 : 0))
#define ptest_cell(life_pr,col) ((life_pr[(col) >> 3] & (0x80 >> ((col) & 0x07)) ? 1 : 0))
#define ptest_cell_pc(life_pr,col) ((life_pr[(col-1) >> 3] & (0x80 >> ((col-1) & 0x07)) ? 1 : 0))

#define test_cell_nc(life_cr,col) ((life_cr[(col+1) >> 3] & (0x80 >> ((col+1) & 0x07)) ? 1 : 0))
#define test_cell_pc(life_cr,col) ((life_cr[(col-1) >> 3] & (0x80 >> ((col-1) & 0x07)) ? 1 : 0))

#define ntest_cell_nc(life_nr,col) ((life_nr[(col+1) >> 3] & (0x80 >> ((col+1) & 0x07)) ? 1 : 0))
#define ntest_cell(life_nr,col) ((life_nr[(col) >> 3] & (0x80 >> ((col) & 0x07)) ? 1 : 0))
#define ntest_cell_pc(life_nr,col) ((life_nr[(col-1) >> 3] & (0x80 >> ((col-1) & 0x07)) ? 1 : 0))

#define test_cell(row,col) life[row][col >> 3] & (0x80 >> ((col) & 0x07))
#define lcd_birth(row,col) lcd_point((col) << 1, (row) << 1, 7)
#define lcd_death(row,col) lcd_point((col) << 1, (row) << 1, 6)

#endif /* LIFE_H_ */
