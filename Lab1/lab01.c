//	Lab01.c	2014/06/16
#include <msp430.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "lab01.h"

unsigned long MCLK_HZ = 1050000;		// SMCLK frequency in Hz
unsigned long BPS = 9600;				// ASYNC serial baud rate

int main(void)
{
    signed char biggest_char, next_char, smallest_char;
    unsigned char un_biggest_char, un_next_char;

    signed int biggest_int, next_int, smallest_int;
    unsigned int un_biggest_int, un_next_int;

    signed long biggest_long, next_long, smallest_long;
    unsigned long un_biggest_long, un_next_long;

	lab01_init();

	// uncomment call to find_baud_rate to find MCLK_HZ
	// find_baud_rate();

	TERMINAL("\n\r** INTEGERS *************");


  	// FINDING THE INTS

  	next_int = 1;
  	biggest_int = 1;

  	while((next_int <<= 1) > 0)
  	{
  		biggest_int = (biggest_int << 1) + 1;;
  	}

  	TERMINAL2("Largest signed int = %d (0x%2x)", biggest_int);

  	next_int = -1;
  	smallest_int = 0;

  	while((next_int <<= 0) < smallest_int)
  	{
  		smallest_int = (smallest_int << 0) + 1;;
  	}

  	TERMINAL2("Most negative signed int = %d (0x%2x)", smallest_int);

  	un_next_int = 1;
  	un_biggest_int = 1;

  	while((un_next_int <<= 1) > un_biggest_int)
  	{
  		un_biggest_int = (un_biggest_int << 1) + 1;;
  	}

  	TERMINAL2("Largest unsigned int = %u (0x%2x)", un_biggest_int);

  	TERMINAL("\n\r** OVERFLOW WITH INTS *************");
  	TERMINAL2("Added to largest signed int = %d (0x%2x)", (biggest_int + 1));
  	TERMINAL2("Subtracted to most negative signed int = %d (0x%2x)", (smallest_int - 1));
  	TERMINAL2("Added to largest unsigned int = %u (0x%2x)", (un_biggest_int + 1));


  	TERMINAL("\n\r** LONGS *************");
  	// FINDING THE LONGS

  	next_long = 1;
  	biggest_long = 1;

  	while((next_long <<= 1) > 0)
  	{
  		biggest_long = (biggest_long << 1) + 1;;
  	}

  	TERMINAL2("Largest signed long = %ld (0x%2x)", biggest_long);


  	smallest_long = -1;
  	next_long = -1;

  	while((next_long <<= 1) < 0)
  	{
  		smallest_long = (smallest_long << 1);;
  	}

  	TERMINAL2("Most negative signed long = %ld (0x%2x)", smallest_long);


  	un_next_long = 1;
  	un_biggest_long = 1;

  	while((un_next_long <<= 1) > un_biggest_long)
  	{
  		un_biggest_long = (un_biggest_long << 1) + 1;;
  	}

  	TERMINAL2("Largest unsigned long = %lu (0x%2x)", un_biggest_long);

  	TERMINAL("\n\r** OVERFLOW WITH LONGS *************");
  	TERMINAL2("Added to largest signed long = %ld (0x%2x)", (biggest_long + 1));
  	TERMINAL2("Subtracted to most negative signed long = %ld (0x%2x)", (smallest_long - 1));
  	TERMINAL2("Added to largest unsigned long = %lu (0x%2x)", (un_biggest_long + 1));


  	TERMINAL("\n\r** CHARS *************");
  	// FINDING THE CHARS

    next_char = 1;						// start w/1
    biggest_char = 1;

    while((next_char <<= 1) > 0)
    {
    	biggest_char = (biggest_char << 1) + 1;;
    }

	TERMINAL2("Largest signed char = %d (0x%2x)", biggest_char);

	next_char = -1;
	smallest_char = -1;

	while((next_char <<= 1) < 0)
	{
		smallest_char = (smallest_char << 1);;
	}

	TERMINAL2("Most negative signed char = %d (0x%2x)", smallest_char);

	un_next_char = 1;
	un_biggest_char = 1;

	while((un_next_char <<= 1) > un_biggest_char)
	{
		un_biggest_char = (un_biggest_char << 1) + 1;;
	}

	TERMINAL2("Largest unsigned char = %u (0x%2x)", un_biggest_char);



	TERMINAL("\n\r** OVERFLOW WITH CHARS *************");
	TERMINAL2("Added to largest signed char = %ld (0x%2x)", (biggest_char + 1));
	TERMINAL2("Subtracted to most negative signed char = %ld (0x%2x)", (smallest_char - 1));
	TERMINAL2("Added to largest unsigned char = %lu (0x%2x)", (un_biggest_char + 1));



	// FIXED POINT NUMBERS


	TERMINAL3("Largest Q16.16 = %f (0x%2x)", (((long)biggest_long)/65536.0), biggest_long);
	TERMINAL3("Most negative Q16.16 = %f (0x%2x)", (((long)smallest_long)/65536.0), smallest_long);


	// FLOATING POINT NUMBERS

	TERMINAL("\n\r** FLOATING POINT ROUNDOFF AND PROBLEMS WITH EQUALITY COMPARISONS *************");

	float aNumb1 = 0.000005f;
	float aSum1 = 0.0;

	float aNumb2 = 0.0025f;
	float aSum2 = 0.0;

	float f = 0.003f;
	float sum = 0.0;
	int i;

	for (i = 0; i < 1000; ++i){
		sum += f;
		aSum1 += aNumb1;
		aSum2 += aNumb2;

	}
	TERMINAL2("First Sum = %f (0x%04lx)", sum);
	TERMINAL2("First multiply = %f (0x%04lx)", f * 1000);
	TERMINAL2("Second Sum = %f (0x%04lx)", aSum1);
	TERMINAL2("Second multiply = %f (0x%04lx)", aNumb1 * 1000);
	TERMINAL2("Third Sum = %f (0x%04lx)", aSum2);
	TERMINAL2("Third multiply = %f (0x%04lx)", aNumb2 * 1000);





	// ORDER OF OPERATIONS

	float sum1 = (0.000000002 + 2) - 2;
	float sum2 = 0.000000002 + (2 - 2);

	TERMINAL2("sum1 = %f (0x%08lx)", sum1);
	TERMINAL2("sum2 = %f (0x%08lx)", sum2);

	sum1 = (0.564267755 + 5) - 5;
	sum2 = 0.564267755 + (5 - 5);

	TERMINAL2("sum1 = %f (0x%08lx)", sum1);
	TERMINAL2("sum2 = %f (0x%08lx)", sum2);





	TERMINAL("\n\r** END ******************");
    return 0;
}
