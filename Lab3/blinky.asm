;*******************************************************************************
;   CS/ECEn 124 Lab 3 - blinky.asm: Software Toggle P1.0
;
;   Description: Quickly blink P1.0 every 10 seconds.  Calculate MCLK, CPI, MIPS
;        Author: Kaiden Nunes
; 		THIS IS MY WORK
;
;             MSP430G5223
;             -----------
;            |       P1.0|-->LED1-RED LED
;            |       P1.3|<--S2
;            |       P1.6|-->LED2-GREEN LED
;
; Show all calculations:
;
;   MCLK = 10260828 cycles / 10 second interval = 1.02608 Mhz
;    CPI = 10260828 cycles/ 6840518 instructions =  1.50001 Cycles/Instruction
;   MIPS = MCLK / CPI / 1000000 =  6.840518e-7 MIPS

;
;*******************************************************************************
           .cdecls  C,"msp430.h"            ; MSP430

COUNT      	.equ    34099                   ; delay count
COUNTING    .equ    101                  	; delay outer loop count
BLINK		.equ	10000					; blink delay count
;------------------------------------------------------------------------------
            .text                           ; beginning of executable code
;------------------------------------------------------------------------------
start:      mov.w   #0x0280,SP              ;    init stack pointer 2w, 2c
            mov.w   #WDTPW|WDTHOLD,&WDTCTL  ;    stop WDT 2w, 5c
            bis.b   #0x01,&P1DIR            ;  set P1.0 as output 2w, 4c

mainloop:   bis.b	#0x01,&P1OUT			; 2w, 4c TURN ON LED
			mov.w	#BLINK,r4				; 2w, 1c for (r4 = BLINK; r4 <> 0; r4--);
bloop:		sub.w	#1,r4					; 1w, 1c
				jne	bloop					; 1w, 2c
			bic.b	#0x01,&P1OUT			; 2w, 4c TURN OFF LED
			mov.w   #COUNTING,r5            ; 2w, 1c use R5 as delay counter

outerloop:	mov.w   #COUNT,r15              ; 2w, 1c use R15 as delay counter
			sub.w   #1,r5                 	;    delay over? 1w,1c
			  jeq   mainloop                ;    return to main loop 1w, 2c
			call	#delayloop              ; call subroutine 1w, 2c
            jmp     outerloop               ;    y, toggle led 1w, 2c
;------------------------------------------------------------------------------
;			Subroutine
;------------------------------------------------------------------------------
delayloop:  sub.w   #1,r15                  ;    delay over? 1w,1c
              jne   delayloop               ;    n 1w, 2c
            ret				                ;    return to outerloop 1w, 2c
;------------------------------------------------------------------------------
;           Interrupt Vectors
;------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .word   start                   ; start address
            .end
